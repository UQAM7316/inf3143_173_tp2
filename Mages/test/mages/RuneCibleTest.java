/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mages;

import org.junit.Test;
import static org.junit.Assert.*;

public class RuneCibleTest {
    
    @Test
    public void runeCible_coutMana_egal10() {
        RuneCible instance = new RuneCible();
        Integer expResult = 10;
        Integer result = instance.coutMana();
        assertEquals(expResult, result);
    }
    
    @Test
    public void runeCible_poids_egal10() {
        RuneCible instance = new RuneCible();
        Integer expResult = 10;
        Integer result = instance.poids();
        assertEquals(expResult, result);
    }
    
    @Test
    public void runeCible_toString() {
        RuneCible instance = new RuneCible();
        String expResult = "rune cible";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
