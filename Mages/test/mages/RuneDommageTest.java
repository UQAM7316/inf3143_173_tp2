/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mages;

import org.junit.Test;
import static org.junit.Assert.*;

public class RuneDommageTest {
      
    @Test(expected = IllegalArgumentException.class)
    public void runeDommage_creerNouvelleRune_dommagesInvalides() {
        RuneDommage instance = new RuneDommage(0);
    }
    
    @Test
    public void runeDommage_coutMana_egal10() {
        RuneDommage instance = new RuneDommage(10);
        Integer expResult = 10;
        Integer result = instance.coutMana();
        assertEquals(expResult, result);
    }
    
    @Test
    public void runeDommage_coutMana_negatifEgal10() {
        RuneDommage instance = new RuneDommage(-10);
        Integer expResult = 10;
        Integer result = instance.coutMana();
        assertEquals(expResult, result);
    }

    @Test
    public void runeDommage_poids_dommagesPlus10() {
        RuneDommage instance = new RuneDommage(10);
        Integer expResult = 20;
        Integer result = instance.poids();
        assertEquals(expResult, result);
    }

    @Test
    public void runeDommage_toString_soin() {
        RuneDommage instance = new RuneDommage(10);
        String expResult = "rune soin (soin: 10)";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
    @Test
    public void runeDommage_toString_dommages() {
        RuneDommage instance = new RuneDommage(-10);
        String expResult = "rune dommages (dommages: -10)";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    @Test
    public void runeDommages_getDommages() {
        RuneDommage instance = new RuneDommage(-10);
        Integer expResult = -10;
        Integer result = instance.getDommages();
        assertEquals(expResult, result);
    }

    @Test
    public void runeDommages_estSoin_true() {
        RuneDommage instance = new RuneDommage(10);
        Boolean result = instance.getSoin();
        assertTrue(result);
    }
    
    @Test
    public void runeDommages_estSoin_false() {
        RuneDommage instance = new RuneDommage(-10);
        Boolean result = instance.getSoin();
        assertFalse(result);
    }
    
}
