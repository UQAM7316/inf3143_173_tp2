/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mages;

import static org.junit.Assert.*;
import org.junit.Test;

public class BatonTest {

    @Test
    public void baton_creerNouveauBaton_ok() {
        Baton instance = new Baton(1);
        Integer expResult = 1;
        Integer result = instance.getNiveau();
        assertEquals(expResult, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void baton_creerNouveauBaton_erreurNiveauZero() {
        Baton instance = new Baton(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void baton_creerNouveauBaton_erreurNiveauNegatif() {
        Baton instance = new Baton(-1);
    }

    @Test
    public void baton_toString() {
        Baton instance = new Baton(10);
        String expResult = "bâton (niveau: 10)";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

}
