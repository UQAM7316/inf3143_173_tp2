/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package contrats;

import mages.Baton;
import mages.Mage;
import mages.Objet;

public class TestMage35 {
   
    public static void main(String[] args) {
        Mage mage = new Mage35("test", 1);
        Baton baton = new Baton(1);
        mage.equipe(baton);
        mage.desequipe(baton);
    }
}

class Mage35 extends Mage {

    public Mage35(String nom, Integer niveau) {
        super(nom, niveau);
    }

    @Override
    public void desequipe(Objet objet) {

    }

}