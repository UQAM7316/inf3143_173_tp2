/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mages;

/**
 * Les runes permettent aux mages de préparer des sorts.
 *
 * Une rune est un objet.
 */
public abstract class Rune implements Objet {
    
    /**
     * Le nombre de points de mana nécessaires pour utiliser cette rune dans un sort.
     * 
     * @return Le coût de la rune en mana.
     */
    public abstract Integer coutMana();
    
    /**
     * Toutes les runes ont un poids de base de `10`.
     *
     * @return 10.
     */
    @Override
    public Integer poids() {
        return 10;
    }

}
