/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mages;

import java.util.Set;

/**
 * Un mage capable de lancer des sorts.
 */
public class Mage {

    /**
     * Le nom du mage.
     */
    private String nom;

    /**
     * Le niveau du mage.
     *
     * Au minimum 1.
     */
    private Integer niveau;

    /**
     * Crée un nouveau mage.
     *
     * @param nom Le nom du nouveau mage.
     * @param niveau Le niveau du nouveau mage.
     */
    public Mage(String nom, Integer niveau) {
        this.nom = nom;
        this.niveau = niveau;
        this.vie = niveau * 100;
        this.mana = niveau * 100;
    }

    /**
     * Le nombre de points de vie du mage.
     */
    private Integer vie;

    /**
     * Le nombre de points de mana du mage.
     */
    private Integer mana;

    /**
     * Le bâton équipé par le mage s'il en a un.
     */
    private Baton baton = null;

    /**
     * Le sac équipé par le mage s'il en a un.
     */
    private Sac sac = null;

    /**
     * Fait équiper un objet par le mage.
     *
     * L'objet doit être un sac ou un bâton.
     *
     * @param objet L'objet à équiper.
     */
    public void equipe(Objet objet) {
        System.out.println(this + " équipe " + objet);
        if (objet instanceof Sac) {
            this.sac = (Sac) objet;
        } else if (objet instanceof Baton) {
            this.baton = (Baton) objet;
        }
    }

    /**
     * Fait déséquiper un objet par le mage.
     *
     * L'objet doit être un sac ou un bâton.
     *
     * @param objet L'objet à équiper.
     */
    public void desequipe(Objet objet) {
        System.out.println(this + " déséquipe " + objet);
        if (objet == this.sac) {
            this.sac = null;
        } else if (objet == this.baton) {
            this.baton = null;
        }
    }

    /**
     * Ramasse un objet et le place dans le sac du mage.
     *
     * @param objet L'objet à ramasser.
     *
     * @throws SacException Si le sac ne peut contenir l'objet.
     */
    public void ramasse(Objet objet) throws SacException {
        System.out.println(this + " ramasse " + objet);
        sac.ajoute(objet);
    }

    /**
     * Prépare un nouveau sort.
     *
     * @param runes Les runes à placer dans le sort.
     * @param cibles Les cibles du sort.
     *
     * @return Le sort une fois créé.
     */
    public Sort prepareSort(Set<Rune> runes, Set<Mage> cibles) {
        System.out.println(this + " prépare un sort");
        return new Sort(runes, cibles);
    }

    /**
     * Lancer le sort préparé avec `preparerSort`.
     *
     * @param sort Le sort à lancer.
     */
    public void lanceSort(Sort sort) {
        System.out.println(this + " lance un sort");
        sort.lancer();
        mana -= sort.coutMana();
    }

    /**
     * Retire des points de vie au mage.
     *
     * @param dommages Le nombre de points à retirer.
     */
    public void blesse(Integer dommages) {
        System.out.println(this + " perd " + dommages + " points de vie");
        this.vie -= dommages;
        if (this.vie < 0) {
            this.vie = 0;
        }
    }

    /**
     * Ajoute des points de vie au mage.
     *
     * @param vie Le nombre de points à ajouter.
     */
    public void soigne(Integer vie) {
        System.out.println(this + " gagne " + vie + " points de vie");
        this.vie += vie;
        if (this.vie > niveau * 100) {
            this.vie = niveau * 100;
        }
    }

    /**
     * Est-ce que le mage possède l'objet?
     *
     * @param objet L'objet à chercher.
     * @return Vrai si le mage possède l'objet.
     */
    public Boolean possede(Objet objet) {
        if (objet == this.sac) {
            return true;
        }
        if (objet == this.baton) {
            return true;
        }
        if (sac != null) {
            return sac.contient(objet);
        }
        return false;
    }

    @Override
    public String toString() {
        return nom;
    }

    /**
     * Retourne une chaîne de caractères avec le niveau, la vie et la mana du
     * mage.
     *
     * Utilisé essentiellement pour le déboguage.
     *
     * @return La chaîne de status.
     */
    public String status() {
        return this + " (niveau: " + niveau + ", vie: " + vie + ", mana: " + mana + ")";
    }

    public String getNom() {
        return nom;
    }

    public Integer getNiveau() {
        return niveau;
    }

    public void setNiveau(Integer niveau) {
        this.niveau = niveau;
    }

    public Integer getVie() {
        return vie;
    }

    public void setVie(Integer vie) {
        this.vie = vie;
    }

    public Integer getMana() {
        return mana;
    }

    public void setMana(Integer mana) {
        this.mana = mana;
    }

    public Sac getSac() {
        return sac;
    }

    public Baton getBaton() {
        return baton;
    }

}
