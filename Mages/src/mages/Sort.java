/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mages;

import java.util.Set;

/**
 * Un sort à lancer.
 */
public class Sort {

    /**
     * Runes constituant le sort.
     */
    private Set<Rune> runes;

    /**
     * Cibles du sort s'il y'en a.
     */
    private Set<Mage> cibles;

    /**
     * Créer un nouveau sort.
     *
     * Visibilité package pour empêcher toute construction en dehors de
     * `Mage::preparerSort`.
     */
    Sort(Set<Rune> runes, Set<Mage> cibles) {
        this.runes = runes;
        this.cibles = cibles;
    }

    /**
     * Exécute le sort.
     */
    public void lancer() {
        Integer dommages = this.dommages();
        for (Mage mage : cibles) {
            if (dommages <= 0) {
                mage.blesse(Math.abs(dommages));
            } else {
                mage.soigne(dommages);
            }
        }
    }

    /**
     * Retourne les dommages (ou soins) à appliquer par le sort.
     *
     * @return La somme des valeurs des runes de dommages du sort.
     */
    public Integer dommages() {
        Integer dommages = 0;
        for (Rune rune : runes) {
            if (rune instanceof RuneDommage) {
                dommages += ((RuneDommage) rune).getDommages();
            }
        }
        return dommages;
    }

    /**
     * Retourne le nombre de cibles maximum à impacter par le sort.
     *
     * @return Le nombre de rune de cible dans le sort.
     */
    public Integer nombreCibles() {
        Integer cibles = 0;
        for (Rune rune : runes) {
            if (rune instanceof RuneCible) {
                cibles++;
            }
        }
        return cibles;
    }
    
    /**
     * Retourne le coût en mana du sort.
     * 
     * Le coût du sort est calculé par la somme du coût de chaque rune
     * multiplié par le nombre de cibles.
     * 
     * @return Le coût en mana du sort.
     */
    public Integer coutMana() {
        Integer mana = 0;
        for (Rune rune : runes) {
            mana += rune.coutMana();
        }
        return mana * cibles.size();
    }

    public Set<Rune> getRunes() {
        return runes;
    }

    public Set<Mage> getCibles() {
        return cibles;
    }

}
