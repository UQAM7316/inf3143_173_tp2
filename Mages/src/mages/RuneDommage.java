/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mages;

/**
 * Les runes de dommages permettent de créer des sorts qui soignent ou blessent.
 * 
 * Si la valeur de `dommages` est négative alors le sort blesse.
 * Si la valeur de `dommages` est positive alors le sort soigne.
 */
public class RuneDommage extends Rune {
    
    private Integer dommages;
    
    private Boolean soin = false;

    public RuneDommage(Integer dommages) {
        if(dommages == 0) {
            throw new IllegalArgumentException("`dommages` doit être strictement positif ou négatif");
        }
        this.dommages = dommages;
        soin = dommages > 0;
    }

    /**
     * Une rune de dommage coûte le nombre de points de dommages ou de soins de la rune.
     * 
     * @return La valeur absolue de `dommages`.
     */
    @Override
    public Integer coutMana() {
        return Math.abs(dommages);
    }
    
    /**
     * Le poids d'une rune de dommages dépend de la valeur de `dommages`.
     *
     * @return 10.
     */
    @Override
    public Integer poids() {
        return super.poids() + Math.abs(dommages);
    }

    @Override
    public String toString() {
        if(soin) {
            return "rune soin (soin: " + dommages + ")";
        }
        return "rune dommages (dommages: " + dommages + ")";
    }

    public int getDommages() {
        return dommages;
    }

    public Boolean getSoin() {
        return soin;
    }
    
}
