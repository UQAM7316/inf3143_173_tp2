/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mages;

import java.util.HashSet;
import java.util.Set;

/**
 * Un sac permet à un mage de transporter des objets.
 *
 * Les sacs sont aussi des objets.
 */
public class Sac implements Objet {

    /**
     * La taille représente la quantité de `poids` que le sac peut contenir.
     */
    private Integer taille;

    /**
     * Le poids du sac lorsqu'il est vide.
     */
    private Integer poidsVide;

    /**
     * Crée un nouveau sac vide.
     *
     * @param taille La taille du sac à créer.
     */
    public Sac(Integer taille) {
        if (taille < 10) {
            throw new IllegalArgumentException("`taille` doit être suppérieure ou égale à 10.");
        }
        this.taille = taille;
        this.poidsVide = taille / 10;
    }

    /**
     * Les objets contenus dans le sac.
     */
    Set<Objet> objets = new HashSet<>();

    /**
     * Est-ce que le sac contient `objet`?
     *
     * @param objet L'objet à chercher.
     * @return Vrai si l'objet est dans le sac.
     */
    public Boolean contient(Objet objet) {
        for (Objet autre : objets) {
            if (objet == autre) {
                return true;
            }
            if (autre instanceof Sac) {
                Sac sac = (Sac)autre;
                if(sac.contient(objet)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Place un objet dans le sac.
     *
     * @param objet L'objet à placer dans le sac.
     * @throws SacException Si le sac ne peut contenir l'objet.
     */
    public void ajoute(Objet objet) throws SacException {
        if (objet.poids() + poidsObjets() > taille) {
            throw new SacException("Le sac ne peut contenir cet objet");
        }
        this.objets.add(objet);
    }

    /**
     * Retire un objet du sac.
     *
     * @param objet L'objet à retirer
     * @throws SacException Si le sac ne contient pas l'objet.
     */
    public void retire(Objet objet) throws SacException {
        if (!objets.contains(objet)) {
            throw new SacException("Le sac ne contient pas cet objet");
        }
        this.objets.remove(objet);
    }

    /**
     * Le poids cumulé des objets dans le sac.
     *
     * @return La somme des poids des `objets`.
     */
    public Integer poidsObjets() {
        Integer poids = 0;
        for (Objet objet : objets) {
            poids += objet.poids();
        }
        return poids;
    }

    /**
     * Le poids du sac correspond à son poids à vide plus le poids des objets
     * qu'il contient.
     *
     * @return Le poids du sac.
     */
    @Override
    public Integer poids() {
        return poidsVide + poidsObjets();
    }

    /**
     * Retourne le poids du sac à vide.
     *
     * @return Le poids du sac vide.
     */
    public Integer getPoidsVide() {
        return poidsVide;
    }

    /**
     * Retourne la taille du sac.
     *
     * @return La taille du sac.
     */
    public Integer getTaille() {
        return taille;
    }
    
    /**
     * Retourne les objets contenus dans le sac.
     *
     * @return Les objets dans le sac.
     */
    public Set<Objet> getObjets() {
        return objets;
    }
    
    @Override
    public String toString() {
        return "sac (taille: " + taille + ")";
    }
}
