/*
 * Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mages;

/**
 * Bâton magique.
 *
 * Un bâton peut-être équipé par un mage pour lancer des sorts.
 */
public class Baton implements Objet {

    /**
     * Le niveau du bâton.
     */
    private Integer niveau;

    /**
     * Crée un nouveau bâton.
     *
     * @param niveau Le niveau du bâton à créer.
     */
    public Baton(Integer niveau) {
        if (niveau < 1) {
            throw new IllegalArgumentException("`niveau` doit être structement positif");
        }
        this.niveau = niveau;
    }

    /**
     * Retourne le niveau du bâton.
     *
     * @return Le niveau du bâton.
     */
    public Integer getNiveau() {
        return niveau;
    }
    
    /**
     * Les bâtons pèsent tous 200 unités de poids.
     * 
     * @return 200.
     */
    @Override
    public Integer poids() {
        return 200;
    };

    @Override
    public String toString() {
        return "bâton (niveau: " + niveau + ")";
    }

}
