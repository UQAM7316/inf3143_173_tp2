# Copyright 2017 Alexandre Terrasa <alexandre@moz-code.org>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

.PHONY: all build-out build-src build-contrats build-tests run-main check check-main check-contrats check-tests clean

JUNIT = Mages/lib/junit-4.12.jar:Mages/lib/hamcrest-core-1.3.jar
COFOJA = Mages/lib/cofoja.asm-1.3-20160207.jar

SRC = $(wildcard Mages/src/mages/*.java)
TESTS = $(wildcard Mages/test/mages/*.java)
CONTRATS = $(wildcard Mages/test/contrats/*.java)

OUT = bin/

all: check

build-out:
	mkdir -p $(OUT)

build-src: build-out $(SRC)
	javac -d $(OUT) -cp Mages/src:$(COFOJA) $(SRC)

build-contrats: build-out build-src $(CONTRATS)
	javac -d $(OUT) -cp $(OUT):$(COFOJA) $(CONTRATS)

build-tests: build-out build-src $(TESTS)
	javac -d $(OUT) -cp $(OUT):$(COFOJA):$(JUNIT) $(TESTS)

run-main: build-src
	java -cp bin/ mages.Main

check: check-main check-contrats check-tests

check-main: build-src
	echo "# Testing MAIN"
	./check_main.sh

check-contrats: build-contrats
	echo "# Testing CONTRATS"
	./check_contrats.sh

check-tests: build-tests
	echo "# Testing JUNIT"
	./check_tests.sh

clean:
	rm -rf $(OUT) out/
